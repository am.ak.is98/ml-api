# ML-API

## Content

- [API overview](#api-overview)
- [Accessing API](#accessing-api)
- [Curl request for testing](#curl-request-for-testing)
- [Classification model](#classification-model)
- [Questions](#questions)

## Important Note

I built the model using the mentioned approach but I couldn't import it due to problems in versions, so I replaced it with a naive algorithm that depends on the phone number format. (If phone number is numeric and its length is equal to 10, it's not abuser; anything else will be classified as abuser).

## API overview

FastAPI endpoint for abusers classification.

The API contains 1 endpoint `/classify_abuser` that uses a POST method to classify users into abusers or not based on phone number and its length.

### Accessing API:

1. Click on this [link](https://8000-amakis98-mlapi-wld950vqvi3.ws-eu111.gitpod.io/classify_abuser/).

2. Replace `<string>` with proper phone number.

3. Click Execute

#### Curl request for testing

```
curl -X 'POST' \
  'https://8000-amakis98-mlapi-wld950vqvi3.ws-eu111.gitpod.io/classify_abuser/' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "phone": "0123456789"
}'
```


### Classification model

I have used Random Forest classifier with n_estimators = 100.

**Model building steps**:

- Dataset preparation
- Feature engineering (Age, longitude, latitude)
- Handling imbalanced data
- Scoring (F1-score)

### Questions
- **What metrics will you use to evaluate the performance of the Statistical solution/or AI model?** <br>
  F1-Score: Monitor the F1-score over time to assess the model's overall performance, especially in this case where precision and recall are both important.
- **How will you ensure the security of the deployed API?** <br>
  Send the data from the front end to the backend and call the api from the backend server and configure only backend server ip as a whitelisted ip.
- **What steps will you take to monitor the performance and reliability of the deployed API?** <br>
  Perform the following tests 
  1. Load Testing: Conduct regular load tests to assess the API's performance under different levels of  load and traffic. 
  2. Health Checks: Implement health checks to monitor the health and availability of the API. 
  3. Scalability Planning: Monitor the API's usage patterns and performance trends to anticipate future scalability needs.
- **How will you handle potential errors or failures in the ETL pipeline or API deployment?** <br>
  1. Monitoring and Alerting: Implement robust monitoring and alerting mechanisms to detect errors or failures in real-time.
  2. Error Logging: Log errors and exceptions at each stage of the ETL pipeline and API deployment. 
  3. Root causes Analysis: Perform analysis following incidents or failures to identify root causes, lessons learned, and opportunities for improvement. Document findings and implement corrective actions to prevent similar issues from occurring in the future.