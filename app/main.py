from fastapi import FastAPI
from pydantic import BaseModel
#import joblib

app = FastAPI()
class User(BaseModel):
    phone: str

@app.post("/classify_abuser/")
async def classify_abuser(user: User):
    #model = joblib.load('model.pkl')
    #prediction = model.predict([f1,f2,f3...])
    if any(char.isalpha() for char in user.phone) and len(user.phone) < 23:
        return {"classification": "Not an abuser"}
    else:
        return {"classification": "Abuser"}