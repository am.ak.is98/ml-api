from fastapi import FastAPI
import uvicorn

# Import your FastAPI application from main.py
from app.main import app

if __name__ == "__main__":
    # Start the FastAPI application using Uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)
